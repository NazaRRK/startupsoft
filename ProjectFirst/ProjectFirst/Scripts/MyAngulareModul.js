﻿var myApp = angular.module("myApp", [])

myApp.controller("CommentCtrl", function ($scope, $http, $document) {
    //Data-send
    $scope.send = {
        id: null,
        name: null,
        date: null,
        title: null,
        description: null
    }

    //Data-udate
    $scope.update = {
        id: null,
        name: null,
        date: null,
        title: null,
        description: null
    }

    //Function send-comment
    $scope.send_comment = function (id, name, date, title, description) {
        var time = new Date();
        date = time.getDate() + "." + (time.getMonth() + 1) + "." + (time.getYear() + 1900);
        $http.post("Index", { id, name, date, title, description }).success(function (data) {
            $("#result").html(data);
        })
        $scope.send.name = null;
        $scope.send.title = null;
        $scope.send.descr = null;
    }

    //Click edit-button
    $document.on("click", ".edit_button", function (event) {
        event.preventDefault();
        $scope.update.id = $(this).val();
        var id_all = "#com_" + $scope.update.id;
        $scope.update.name = $("#name", id_all).text();
        $scope.update.title = $("#title", id_all).text();
        $scope.update.description = $(".descriptionblock", id_all).text();
        $scope.$apply();
        $('#overlay').fadeIn(400,function () {
		 	    $('#modal_form')
					.css('display', 'block')
					.animate({ opacity: 1, top: '50%' }, 200);
		 	});
    });

    //Click delete-button
    $document.on("click", ".delete_button", function (event) {
        var id = $(this).val();
        $http.post("Delete", { id }).success(function () {
            $("#com_" + id).remove();
        })
    });

    //Click close modale window
    $scope.close_edit = function () {
        $('#modal_form').animate({ opacity: 0, top: '45%' }, 200,
				function () {
				    $(this).css('display', 'none');
				    $('#overlay').fadeOut(400);
				}
			);
    }
    
    //Click save edit
    $scope.save_edit = function (id, name, date, title, description) {
        var time = new Date();
        date = time.getDate() + "." + (time.getMonth() + 1) + "." + (time.getYear() + 1900);
        $http.post("SaveEdit", { id, name, date, title, description }).success(function (data) {
            $("#result").html(data);
        })
        $('#modal_form').animate({ opacity: 0, top: '45%' }, 200,
				function () {
				    $(this).css('display', 'none');
				    $('#overlay').fadeOut(400);
				}
			);
    }
})