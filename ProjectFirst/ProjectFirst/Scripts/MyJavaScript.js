﻿
//Button Edit
$(document).on("click", ".edit_button", function (event) {
        event.preventDefault();
        var id_number = $(this).attr('value')
        var id_all = "#com_" + id_number;
        $('#id_ed').val(id_number);
        $('#name_ed').val($("#name", id_all).text());
        $('#title_ed').val($("#title", id_all).text());
        $('#descr_ed').val($(".descriptionblock", id_all).text());
        $('#overlay').fadeIn(400, 
		 	function(){
		 	    $('#modal_form')
					.css('display', 'block')
					.animate({ opacity: 1, top: '50%' }, 200);
		 	});
    });

//Close pop up
    $(document).on( "click", '#modal_close, #overlay',  function(){ 
        $('#modal_form').animate({opacity: 0, top: '45%'}, 200, 
				function(){
				    $(this).css('display', 'none');
				    $('#overlay').fadeOut(400);
				}
			);
    });


//Button save in pop up
    $(document).on("click", '#submit_ed', function () {
        var id_ = $("#id_ed").val();
        var name_ = $("#name_ed").val();
        var date_ = $("#date_ed").val();
        var title_ = $("#title_ed").val();
        var descr_ = $("#descr_ed").val();

        $.ajax({
            url: "SaveEdit",
            type: "post",
            data: { id: id_, name: name_, date: date_, title: title_, description: descr_ },
            success: function (data) {
                $("#result").html(data);
            }
        });

        $('#modal_form').animate({ opacity: 0, top: '45%' }, 200,
				function () {
				    $(this).css('display', 'none');
				    $('#overlay').fadeOut(400);
				}
			);
    });

//Button for comments
    $(document).on("click", '#submit', function () {
        var id_ = $("#id_f").val();
        var name_ = $("#name_f").val();
        var date_ = $("#date_f").val();
        var title_ = $("#title_f").val();
        var descr_ = $("#descr_f").val();
        $.ajax({
            url: "Index",
            type: "post",
            data: { id: id_, name: name_, date: date_, title: title_, description: descr_ },
            success: function (data) {
                $("#result").html(data);
            }
        });
        $("#name_f").val("");
        $("#title_f").val("");
        $("#descr_f").val("");
    });

//Button delete
    $(document).on("click", ".delete_button" ,function () {
        var id_ = ($(this).val()).replace("com_", "");
        $.ajax({
            url: "Delete",
            type: "post",
            data: { id: id_ },
            success: function () {
                $("#com_" + id_).remove();

            }
        });
    });
