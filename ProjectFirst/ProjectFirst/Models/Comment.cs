﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectFirst.Models
{
    public class Comment
    {
        public int id { get; set; }
        public string name { get; set; }
        public string date { get; set; }
        public string title { get; set; }
        public string description { get; set; }

    }
}