﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectFirst.Models;
using System.Data.Entity;

namespace ProjectFirst.Controllers
{
    public class HomeController : Controller
    {
        CommentContext db = new CommentContext();

        public ActionResult Index()
        {
            return View(db.Comments);
        }

        [HttpPost]
        public PartialViewResult Index(Comment comment)
        {
            comment.id = 5;
            db.Comments.Add(comment);
            db.SaveChanges();
            return PartialView("Add", db.Comments.ToList());
        }
        
        [HttpPost]
        public void Delete(int id)
        {
            Comment b = db.Comments.Find(id);
            if (b != null)
            {
                db.Comments.Remove(b);
                db.SaveChanges();
            }
        }

        [HttpPost]
        public PartialViewResult SaveEdit(Comment comment)
        {
            db.Entry(comment).State = EntityState.Modified;
            db.SaveChanges();
            return PartialView("Add", db.Comments.ToList());
        }

    }
}